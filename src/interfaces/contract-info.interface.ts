
export interface ContractInfo {
    symbol: string,
    decimals: number,
    name: string,
    contractAbi: any,
    tokenContract: any
}