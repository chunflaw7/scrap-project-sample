import Web3 from 'web3';
import {Contract} from 'web3-eth-contract';
import axios from 'axios';
import { ContractInfo } from '../interfaces/contract-info.interface';

export class ContractHelper {

    static getTokenContractInfo = async (web3: Web3, contractAddress: string): Promise<ContractInfo> => {
        const contractAbi = await ContractHelper.getContractAbi(contractAddress);
        const tokenContract = new web3.eth.Contract(contractAbi, contractAddress);
        const symbol = await tokenContract.methods.symbol().call();
        const decimals = await tokenContract.methods.decimals().call();
        const name = await tokenContract.methods.name().call();
        return {
            symbol,
            decimals,
            name,
            contractAbi,
            tokenContract
        };
    }

    static getContract = async (web3: Web3, contractAddress: string, abiAddress: string): Promise<Contract> => {
        const abi = await this.getContractAbi(abiAddress);
        const contract = new web3.eth.Contract(abi, contractAddress);
        return contract;
    }
    
    static getContractAbi = async (contractAddress: string): Promise<any> => {
        const ContractAbiApi = `https://api.bscscan.com/api?module=contract&action=getabi&address=${contractAddress}&format=raw`;
        const result = await axios.get(ContractAbiApi);
        // console.log(result.data);
        if (result.data) {
            return result.data?.result ? JSON.parse(result.data.result) : result.data;
        } else {
            return null;
        }
    }

}