import Web3 from 'web3';
import { Contract } from 'web3-eth-contract';
import { ContractHelper } from './helpers/contract-helper';

const BSC_RPC = 'https://bsc-dataseed1.defibit.io';

const playerContractAddress = '0xb00ed7e3671af2675c551a1c26ffdcc5b425359b';
const playerContractAbiAddress = '0x64b6b45e67fbd3a305755458f108fe82dbec988d';

const delay = async (ms: number): Promise<void> => {
    return new Promise((resolve, reject) => setTimeout(() => resolve(), ms));
}

const getRecords = async (web3: Web3, contract: Contract, startBlock: number, toBlockNum: number): Promise<any> => {
    let data = null;
    data = await contract.getPastEvents('Transfer', {
        fromBlock: startBlock,
        toBlock: toBlockNum
    });
    return data;
}

const scrapPlayerContract = async (web3: Web3, contract: any, startBlock: number) => {
    console.log('Getting the current block num...');
    const currentBlockNum: number = await web3.eth.getBlockNumber();
    console.log(`The current bsc block is ${currentBlockNum}`);
    const diff: number = currentBlockNum - startBlock;
    console.log(`The different between current block and start block is ${diff}.`);
    const GET_SIZE: number = 100;
    console.log(`Total ${diff / GET_SIZE} time to be synced.`);

    let currentProcessingBlockNum = startBlock;
    let toBlockNum = (currentProcessingBlockNum + GET_SIZE) < currentBlockNum ? (currentProcessingBlockNum + GET_SIZE) : currentBlockNum;

    while (toBlockNum < currentBlockNum) {
        let results = await getRecords(web3, contract, startBlock, toBlockNum);
        console.log(results.length);
        currentProcessingBlockNum = toBlockNum;
        toBlockNum = currentProcessingBlockNum + GET_SIZE;
    }
}

const init = async () => {
    console.log('Program init...');
    console.log('Getting the contract instance...');
    const web3 = new Web3(BSC_RPC);
    const playerContract = await ContractHelper.getContract(web3, playerContractAddress, playerContractAbiAddress);
    try {
        await scrapPlayerContract(web3, playerContract, 14430991);
    } catch (e) {
        console.log(e);
    }
}

init();
